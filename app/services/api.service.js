/* eslint no-console: 0 */
/* eslint angular/log: 0 */

class ApiService {
  constructor($http) {
    this.$http = $http;
  }

  fetchData() {
    const data = this.$http.get('/api/data');

    return data;
  }

  generateData() {
    const data = this.$http.get('/api/generate');

    return data;
  }

  generateCsv() {
    return this.$http.get('/get-csv');
  }
}

ApiService.$inject = ['$http'];

export default ApiService;
