import forEach from 'lodash/forEach';

class HomeCtrl {
  constructor(ApiService, DataService, $routeParams) {
    this.apiService = ApiService;
    this.dataService = DataService;
    this.isDisabled = false;
    this.changeView = false;
    this.calendarView = true;
    this.data = false;
    this.costs = 0;
    this.days = [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday'
    ];
    this.id = $routeParams.id;

    this.apiService.fetchData().then((data) => {
      this.data = data.data.days;
      this.costs = data.data.costs;
      this.calendar = data.data.days;
      this.marginFirst = `margin-left:${this.data[0].dayOfTheWeek * 120}px`;
      this.isDisabled = false;
      this.changeView = true;
      this.allCosts = 0;
      forEach(this.costs, (el) => {
        this.allCosts += el;
      });
    });
  }

  generateSchedule() {
    this.isDisabled = true;

    this.apiService.generateData().then((data) => {
      this.data = data.data.days;
      this.costs = data.data.costs;
      this.allCosts = 0;
      this.calendar = data.data.days;
      this.marginFirst = `margin-left:${this.data[0].dayOfTheWeek * 120}px`;
      this.isDisabled = false;
      this.changeView = true;
      forEach(this.costs, (el) => {
        this.allCosts += el;
      });
    });
  }

  onChangeView() {
    this.calendarView = !this.calendarView;
  }

  getNurse(id) {
    return this.dataService.getNurse(id);
  }

  getNurses() {
    return this.dataService.getNurses();
  }

  getNurseInitials(id) {
    const nurse = this.getNurse(id);

    return `${nurse.firstName[0]}${nurse.surname[0]}`;
  }

  getDay() {
    return this.data[this.id];
  }
}

HomeCtrl.$inject = ['ApiService', 'DataService', '$routeParams'];

export default HomeCtrl;
