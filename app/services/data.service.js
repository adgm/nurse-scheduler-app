class DataService {
  constructor(config) {
    this.data = config.nurses;
  }

  getNurse(nurseNumber) {
    return this.data[nurseNumber];
  }

  getNurses() {
    return this.data;
  }
}

DataService.$inject = ['config'];

export default DataService;
