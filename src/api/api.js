/* eslint angular/json-functions: 0*/

const router = require('express').Router();
const request = require('request');
const fs = require('fs');

router.get('/generate', (req, res) => {
  request('http://localhost:9090', (error, response, body) => {
    if (!error) {
      fs.writeFile('data.json', body, 'utf-8', () => {
        res.json(JSON.parse(body));
      });
    } else {
      res.sendStatus(500);
    }
  });
});

router.get('/data', (req, res) => {
  const path = 'data.json';

  fs.readFile(path, 'utf-8', (err, body) => {
    if (err) {
      res.sendStatus(500);
    } else {
      res.json(JSON.parse(body));
    }
  });
});

module.exports = router;
